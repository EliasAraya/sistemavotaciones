// Carga inicial de candidatos y regiones.
fetch("http://localhost/votaciones/php/candidatos.php")
  .then((response) => response.json())
  .then((data) => {
    const selectCandidato = document.getElementById("candidato");
    cargarOpcionesSelect(
      selectCandidato,
      data.map((candidato) => ({
        texto: `${candidato.candi_nombre} ${candidato.candi_apellido}`,
        valor: candidato.candi_id,
      })),
      "Seleccione un Candidato"
    );
  })
  .catch((error) => console.error("Error:", error));

fetch("http://localhost/votaciones/php/region.php")
  .then((response) => response.json())
  .then((data) => {
    const selectRegiones = document.getElementById("region");
    cargarOpcionesSelect(
      selectRegiones,
      data.map((region) => ({
        texto: region.reg_nombre,
        valor: region.reg_id,
      })),
      "Seleccione una región"
    );
  })
  .catch((error) => console.error("Error:", error));

/**
 * Carga opciones en un elemento select del DOM.
 * @param {HTMLElement} selectElement - El elemento select que se va a actualizar.
 * @param {Object[]} opciones - Un array de objetos que representan las opciones a cargar.
 * @param {string} textoDefecto - El texto a mostrar para la opción por defecto.
 */
function cargarOpcionesSelect(selectElement, opciones, textoDefecto) {
  selectElement.innerHTML = "";
  selectElement.appendChild(new Option(textoDefecto, ""));

  opciones.forEach((opcion) => {
    selectElement.appendChild(new Option(opcion.texto, opcion.valor));
  });
}

// Evento change que desplegara el listado de comunas de acuerdo a la region seleccionada.
document.getElementById("region").addEventListener("change", function () {
  var regionId = this.value;

  fetch(`http://localhost/votaciones/php/comuna.php?id=${regionId}`)
    .then((response) => response.json())
    .then((data) => {
      const selectComunas = document.getElementById("comuna");
      cargarOpcionesSelect(
        selectComunas,
        data.map((comuna) => ({
          texto: comuna.comu_nombre,
          valor: comuna.comu_id,
        })),
        "Seleccione una comuna"
      );
    })
    .catch((error) => console.error("Error:", error));
});

// Evento submit para el formulario de votación.
document
  .getElementById("votoForm")
  .addEventListener("submit", function (event) {
    event.preventDefault();

    const nombreApellido = document
      .getElementById("nombreApellido")
      .value.trim();
    const alias = document.getElementById("alias").value.trim();
    const rut = document.getElementById("rut").value.trim().toLowerCase();
    const email = document.getElementById("email").value.trim();
    const region = Number(document.getElementById("region").value);
    const comuna = Number(document.getElementById("comuna").value);
    const candidato = Number(document.getElementById("candidato").value);
    const checkboxes = document.querySelectorAll(
      'input[type="checkbox"]:checked'
    );

    const rutNumero = rut.slice(0, -2);
    const dv = rut.slice(-1);
    const rutNumerico = Number(rutNumero);

    if (!nombreApellido) {
      alert("El campo 'Nombre y Apellido' no debe quedar en blanco.");
      return;
    }

    // Validar Alias: Mayor a 5 caracteres y que contenga letras y números.
    const aliasRegex = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+){5,}$/;

    if (!aliasRegex.test(alias)) {
      alert(
        "El 'Alias' debe tener más de 5 caracteres y contener letras y números."
      );
      return;
    }

    const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    if (!emailRegex.test(email)) {
      alert("El 'Email' debe tener un formato valido.");
      return;
    }

    if (checkboxes.length < 2) {
      alert(
        "Debe elegir al menos dos opciones en 'Como se enteró de Nosotros'."
      );
      return;
    }

    if (!validateRut(rut)) {
      alert("El Rut ingresado no es valido.");
      return;
    }

    if (!candidato || !region || !comuna) {
      alert("Todos los campos deben ser llenados.");
      return;
    }

    const votante = {
      nombre_apellido: nombreApellido,
      alias,
      rut: rutNumerico,
      dv,
      email,
      fuente: Array.from(checkboxes).map((cb) => cb.value),
      reg_id: region,
      comu_id: comuna,
      candi_id: candidato,
    };

    fetch("http://localhost/votaciones/php/participantes.php", {
      method: "POST",
      body: JSON.stringify(votante),
    })
      .then((response) => response.json())
      .then((data) => {
        alert(data.message);
        limpiarCampos();
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  });

/**
 * Resetea todos los campos del formulario a sus valores por defecto.
 */
const limpiarCampos = () => {
  document.getElementById("nombreApellido").value = "";
  document.getElementById("alias").value = "";
  document.getElementById("rut").value = "";
  document.getElementById("email").value = "";

  document.getElementById("region").selectedIndex = 0;
  document.getElementById("comuna").selectedIndex = 0;
  document.getElementById("candidato").selectedIndex = 0;

  document.querySelectorAll('input[type="checkbox"]').forEach((checkbox) => {
    checkbox.checked = false;
  });
};

/**
 * Valida el formato del RUT.
 * @param {string} rutcompleto - El RUT completo a validar.
 * @returns {boolean} - Retorna true si el RUT es válido, false en caso contrario.
 */
const validateRut = (rutcompleto) => {
  const rutRegex = /^[0-9]+[-|‐]{1}[0-9kK]{1}$/;
  if (!rutRegex.test(rutcompleto)) return false;
  let tmp = rutcompleto.split("-");
  let digv = tmp[1];
  let rut = tmp[0];
  return checkDV(rut) == digv;
};

/**
 * Calcula el dígito verificador de un RUT.
 * @param {number} T - El número del RUT sin el dígito verificador.
 * @returns {string} - El dígito verificador del RUT.
 */
const checkDV = (T) => {
  let M = 0,
    S = 1;
  for (; T; T = Math.floor(T / 10)) S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
  return S ? S - 1 : "k";
};

// Evento que formatea el rut mientras se escribe : "15444545-3"
document.getElementById("rut").addEventListener("input", function (e) {
  let valor = e.target.value.replace(/[^0-9kK]+/g, "").toUpperCase();

  if (valor.length > 1) {
    valor = valor.slice(0, -1) + "-" + valor.slice(-1);
  }

  e.target.value = valor;
});
