<?php

class conexion{
    private $server = "localhost";
    private $user = "root";
    private $password = "";
    private $database = "sistema_votaciones";
    private $port = "3306";
    
    public $conn;

    public function __construct() {
        $this->conn = new mysqli($this->server, $this->user, $this->password, $this->database, $this->port);
        $this->conn->set_charset("utf8mb4");


        if ($this->conn->connect_errno) {
            echo "Algo va mal con la conexion: " . $this->conn->connect_error;
            die();
        }
    }

    public function getConexion() {
        return $this->conn;
    }




}


?>