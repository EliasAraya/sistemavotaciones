<?php 
require_once "config/conexion.php";
require_once 'class/region.class.php';

$conexion = new conexion;
$db = $conexion->getConexion();

$regiones = new region($db);
header('Access-Control-Allow-Origin: *');
header("Content-Type: application/json");
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        // Llamar al método correspondiente de la clase Region
        $regiones->getAllRegiones();
        break;
}


?>