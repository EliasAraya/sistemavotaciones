<?php 
require_once "config/conexion.php";
require_once 'class/candidatos.class.php';

$conexion = new conexion;
$db = $conexion->getConexion();

$candidato = new candidatos($db);

header('Access-Control-Allow-Origin: *');
header("Content-Type: application/json");
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        // Llamar al método correspondiente de la clase Candidato
        $candidato->getAllCandidatos();
        break;

    case 'POST':
        
        $data = json_decode(file_get_contents("php://input"),true);
        $candidato->create($data);
        break;
}


?>