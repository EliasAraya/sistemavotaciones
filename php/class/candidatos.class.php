<?php

require_once 'config/conexion.php';

class candidatos extends conexion{

    private $db;

    // Constructor para inicializar la conexión a la base de datos
    public function __construct($db) {
        $this->db = $db;
    }

    public function getAllCandidatos() {
        // Lógica para obtener todos los candidatos
        $query = "SELECT * FROM candidatos";
        
        $stmt = $this->db->prepare($query);
        
        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $candidatos = $result->fetch_all(MYSQLI_ASSOC);
            echo json_encode($candidatos);
        } else {
            echo json_encode(["message" => "Error en la consulta: " . $stmt->error]);

        }
    }

    public function create($data) {

        $camposRequeridos = ['nombre', 'apellido', 'rut', 'dv'];

        // Verificar que cada campo requerido esté presente y no esté vacío
        foreach ($camposRequeridos as $campo) {
            if (!isset($data[$campo]) || trim($data[$campo]) === '') {
                echo json_encode(["message" => "Error: el campo '$campo' es requerido y no puede estar vacío."]);
                return;
            }
        }

        // Preparar y ejecutar la consulta para insertar un nuevo candidato
        $query = "INSERT INTO candidatos (candi_nombre, candi_apellido, candi_rut, candi_dv) VALUES (?, ?, ?, ?)";

        $stmt = $this->db->prepare($query);

        if (!$stmt) {
            echo json_encode(["message" => "Error al preparar la consulta"]);
            return;
        }

        $stmt->bind_param("ssss", $data['nombre'], $data['apellido'], $data['rut'], $data['dv']); // 'ssss' representa el tipo de cada dato (s para string)

        if ($stmt->execute()) {
            echo json_encode(["message" => "Candidato creado exitosamente"]);
        } else {
            echo json_encode(["message" => "Error al crear el candidato"]);
        }
    }

}

?>