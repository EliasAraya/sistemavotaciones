<?php 

require_once 'config/conexion.php';

class participantes extends conexion{

    private $db;

    // Constructor para inicializar la conexión a la base de datos
    public function __construct($db) {
        $this->db = $db;
    }

    public function getAllParticipantes() {
        // Lógica para obtener todos los participantes
        $query = "SELECT * FROM participantes";
        $stmt = $this->db->prepare($query);
        
        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $participantes = $result->fetch_all(MYSQLI_ASSOC);
            echo json_encode($participantes);
        } else {
            echo json_encode(["message" => "No se pudieron obtener los participantes."]);
        }
    }

    public function rutDuplicado($rut) {
        $query = "SELECT COUNT(*) FROM participantes WHERE parti_rut = ?";
    
        $stmt = $this->db->prepare($query);
        if (!$stmt) {
            echo json_encode(["message" => "Error al preparar la consulta"]);
            return false;
        }
    
        $stmt->bind_param("i", $rut);
        $stmt->execute();
        $result = $stmt->get_result();
        $row = $result->fetch_array();    
        return $row[0] > 0;
    }
    

    public function create($data) {

        $camposRequeridos = ['nombre_apellido', 'alias', 'rut', 'dv','email','reg_id','comu_id','candi_id'];

        if ($this->rutDuplicado($data['rut'])) {
            echo json_encode(["message" => "Error: Este RUT ya ha votado."]);
            return;
        }

        
        // Verificar que cada campo requerido esté presente y no esté vacío
        foreach ($camposRequeridos as $campo) {
            if (!isset($data[$campo]) || trim($data[$campo]) === '') {
                echo json_encode(["message" => "Error: el campo '$campo' es requerido y no puede estar vacío."]);
                return;
            }
        }

        // Concatenar fuentes (si existen) en una cadena
        $fuente = isset($data['fuente']) ? implode(",", $data['fuente']) : "";


        // Preparar y ejecutar la consulta para insertar un nuevo participante
        $query = "INSERT INTO participantes (parti_nombre_apellido, parti_alias, parti_rut, parti_dv, parti_email, parti_fuente, reg_id, comu_id, candi_id) VALUES (?,?,?,?,?,?,?,?,?)";

        $stmt = $this->db->prepare($query);

        if (!$stmt) {
            echo json_encode(["message" => "Error al preparar la consulta"]);
            return;
        }

        $stmt->bind_param("ssisssiii", $data['nombre_apellido'], $data['alias'], $data['rut'], $data['dv'], $data['email'], $fuente, $data['reg_id'], $data['comu_id'], $data['candi_id']); // 's' representa el tipo de cada dato (s para string)

        if ($stmt->execute()) {
            echo json_encode(["message" => "Voto guardado exitosamente"]);
        } else {
            echo json_encode(["message" => "Error al guardar el voto"]);
        }
    }

}


?>