<?php 

require_once 'config/conexion.php';

class region extends conexion{

    private $db;

    // Constructor para inicializar la conexión a la base de datos
    public function __construct($db) {
        $this->db = $db;
    }

    public function getAllRegiones() {
        // Lógica para obtener todos las regiones
        $query = "SELECT * FROM region";
        $stmt = $this->db->prepare($query);
        
        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $region = $result->fetch_all(MYSQLI_ASSOC);
            echo json_encode($region);
        } else {
            echo json_encode(["message" => "No se pudieron obtener las regiones."]);
        }
    }
    
}


?>