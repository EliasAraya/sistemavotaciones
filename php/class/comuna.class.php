<?php 

require_once 'config/conexion.php';

class comuna extends conexion{

    private $db;

    // Constructor para inicializar la conexión a la base de datos
    public function __construct($db) {
        $this->db = $db;
    }
    
    public function getComunaByRegionId($id) {
        // Lógica para obtener todos las comunas
        $query = "SELECT * FROM comunas WHERE reg_id = ?";
        $stmt = $this->db->prepare($query);

        

        $stmt->bind_param("i", $id);
        
        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $comuna = $result->fetch_all(MYSQLI_ASSOC);

            if($comuna){
                echo json_encode($comuna);
            }else{
                echo json_encode((["message" => "No existen comunas con el id de esta region."]));
            }

        } else {
            echo json_encode(["message" => "No se pudieron obtener las comunas."]);
        }
    }
}


?>