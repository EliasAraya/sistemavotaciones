<?php 
require_once "config/conexion.php";
require_once 'class/participantes.class.php';

$conexion = new conexion;
$db = $conexion->getConexion();

$participantes = new participantes($db);

header('Access-Control-Allow-Origin: *');
header("Content-Type: application/json");
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        // Llamar al método correspondiente de la clase participantes
        $participantes->getAllParticipantes();
        break;

    case 'POST':       
        $data = json_decode(file_get_contents("php://input"),true);
        $participantes->create($data);
        break;
}


?>