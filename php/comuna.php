<?php 
require_once "config/conexion.php";
require_once 'class/comuna.class.php';

$conexion = new conexion;
$db = $conexion->getConexion();

$comuna = new comuna($db);
header('Access-Control-Allow-Origin: *');
header("Content-Type: application/json");
switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':

        
        // Llamar al método correspondiente de la clase comuna
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $comuna->getComunaByRegionId($id);

        break;
}


?>